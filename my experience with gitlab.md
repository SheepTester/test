alarming. gitlab. wow

1. they let you change the navbar theme, but not the colour scheme of the entire page
  - this burns my eyes! not really
2. so many buttons! my brain needs to catch up
3. editor doesn't use the cool dark theme :(
4. how to use soft tabs in editor? and 2 spaces for tab
5. can't change the syntax highlightings :(
6. so different! must get used to
7. not used to putting the static site stuff in `/public` but i guess that's fine
8. took me a while to realise there's a create button in the create label dialogue but you have to scroll down
  - on chromebook so no scrollbar
  - not sure if the scrollbar would be even worse on windows because they're so fat
9. setting the profile picture tries to initially crop a smaller portion of my square image, so i had to zoom out. also, it gives a square to show what'll be kept, but it usually shows your pfp in a cricle
10. just realised "soft wrap" means "make it do soft wrap" and not "currently soft wrap" while writing #9
11. a bit slower, but probably because of the unexpected github mover-inners
12. the number in the buble thing on the issue/merge request thingy on navbar will get distracting one day
13. why do repos need a pfp
14. good marge conflict unconflictifier
15. pressing 'e' doesn't go into edit mode. im too used to it ooff
16. ooh at least they let you copy that commit sha thingy easily. it's a pain on githubby

overall, i will stay on github for now
